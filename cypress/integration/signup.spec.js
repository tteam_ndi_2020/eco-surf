context('SignUpPage', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/signup')
    })

    it('page loaded', () => {
        cy.get('#email').should("have.id", "email")
    })

    it('submit disable by default', () => {
        cy.get('[type=\'submit\']').should("have.attr", "disabled")
    })

    it('submit disable repassword not defined & (test upload)', () => {
        cy.get('#pseudo').type("pseudo")
        cy.get('#email').type("myemail.fr")
        cy.get('#password').type("password")
        cy.fixture('images/logo-vertical.png').as('logo')
        cy.get('input[type=file]').then(function(el) {
            // convert the logo base64 string to a blob
            const blob = Cypress.Blob.base64StringToBlob(this.logo, 'image/png')

            const file = new File([blob], 'images/logo.png', { type: 'image/png' })
            const list = new DataTransfer()

            list.items.add(file)
            const myFileList = list.files

            el[0].files = myFileList
            el[0].dispatchEvent(new Event('change', { bubbles: true }))
        })
        cy.get("[for=\"contained-button-file\"] > .MuiButtonBase-root > .MuiButton-label").should("have.text", "images/logo.png")
        cy.get('[type=\'submit\']').should("have.attr", "disabled")
    })

    it('submit disable password & repassword diff', () => {
        cy.get('#pseudo').type("pseudo")
        cy.get('#email').type("myemail.fr")
        cy.get('#password').type("password")
        cy.get('#repassword').type("repassword")
        cy.fixture('images/logo-vertical.png').as('logo')
        cy.get('input[type=file]').then(function(el) {
            // convert the logo base64 string to a blob
            const blob = Cypress.Blob.base64StringToBlob(this.logo, 'image/png')

            const file = new File([blob], 'images/logo.png', { type: 'image/png' })
            const list = new DataTransfer()

            list.items.add(file)
            const myFileList = list.files

            el[0].files = myFileList
            el[0].dispatchEvent(new Event('change', { bubbles: true }))
        })
        cy.get("[for=\"contained-button-file\"] > .MuiButtonBase-root > .MuiButton-label").should("have.text", "images/logo.png")
        cy.get('[type=\'submit\']').should("have.attr", "disabled")
    })

    it('submit enable', () => {
        cy.get('#pseudo').type("pseudo")
        cy.get('#email').type("myemail.fr")
        cy.get('#password').type("password")
        cy.get('#repassword').type("password")
        cy.fixture('images/logo-vertical.png').as('logo')
        cy.get('input[type=file]').then(function(el) {
            // convert the logo base64 string to a blob
            const blob = Cypress.Blob.base64StringToBlob(this.logo, 'image/png')

            const file = new File([blob], 'images/logo.png', { type: 'image/png' })
            const list = new DataTransfer()

            list.items.add(file)
            const myFileList = list.files

            el[0].files = myFileList
            el[0].dispatchEvent(new Event('change', { bubbles: true }))
        })
        cy.get("[for=\"contained-button-file\"] > .MuiButtonBase-root > .MuiButton-label").should("have.text", "images/logo.png")
        cy.get('[type=\'submit\']').should("not.have.attr", "disabled")
    })

    it('error submit email already exist', () => {
        cy.get('#pseudo').type("pseudo")
        cy.get('#email').type("test@eco-surf.fr")
        cy.get('#password').type("password_password")
        cy.get('#repassword').type("password_password")
        cy.fixture('images/logo-vertical.png').as('logo')
        cy.get('input[type=file]').then(function(el) {
            // convert the logo base64 string to a blob
            const blob = Cypress.Blob.base64StringToBlob(this.logo, 'image/png')

            const file = new File([blob], 'images/logo.png', { type: 'image/png' })
            const list = new DataTransfer()

            list.items.add(file)
            const myFileList = list.files

            el[0].files = myFileList
            el[0].dispatchEvent(new Event('change', { bubbles: true }))
        })
        cy.get("[for=\"contained-button-file\"] > .MuiButtonBase-root > .MuiButton-label").should("have.text", "images/logo.png")
        cy.get('[type=\'submit\']').should("not.have.attr", "disabled")
        cy.get('[type=\'submit\']').click()
        cy.get('#root > main > div > form > p').should("have.text", "The email address is already in use by another account.")
    })
})