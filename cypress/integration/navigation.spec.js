context('Navigation', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/login')
        cy.get('#email').should("have.id", "email")
        cy.get('#email').type("test@eco-surf.fr")
        cy.get('#password').type("password_test")
        cy.get('[type=\'submit\']').click()
        cy.wait(1500)
    })

    it('Session page name', () => {
        cy.url().should('eq', 'http://localhost:8080/sessions')
        cy.get("#root > div > header > div > div").should("have.text", "Test Account")
        cy.get(".MuiToolbar-root > .MuiTypography-root").should("have.text", "Mes sessions")
    })

    it('Navigation', () => {
        cy.url().should('eq', 'http://localhost:8080/sessions')
        cy.get(".PrivateHiddenCss-xsDown-21 > .MuiDrawer-root > .MuiPaper-root > :nth-child(1) > .MuiList-root > :nth-child(1)").click()

        cy.wait(1500)
        cy.url().should('eq', 'http://localhost:8080/feed')
        cy.get(".MuiToolbar-root > .MuiTypography-root").should("have.text", "File d'actualitée")

        cy.get(".PrivateHiddenCss-xsDown-21 > .MuiDrawer-root > .MuiPaper-root > :nth-child(1) > .MuiList-root > :nth-child(3)").click()

        cy.wait(1500)
        cy.url().should('eq', 'http://localhost:8080/stats')
        cy.get(".MuiToolbar-root > .MuiTypography-root").should("have.text", "Statistiques")
    })


    it('Logout', () => {
        cy.url().should('eq', 'http://localhost:8080/sessions')
        cy.get('.PrivateHiddenCss-xsDown-21 > .MuiDrawer-root > .MuiPaper-root > :nth-child(1) > a').click()
        cy.reload()
        cy.wait(1500)
        cy.url().should('eq', 'http://localhost:8080/login')
    })
})