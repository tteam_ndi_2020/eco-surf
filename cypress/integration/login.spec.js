context('Login', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/login')
    })

    it('page loaded', () => {
        cy.get('#email').should("have.id", "email")
    })

    it('submit disable by default', () => {
        cy.get('[type=\'submit\']').should("have.attr", "disabled")
    })

    it('submit disable email not defined', () => {
        cy.get('#email').type("myemail.fr")
        cy.get('[type=\'submit\']').should("have.attr", "disabled")
    })

    it('submit enable', () => {
        cy.get('#email').type("my@email.fr")
        cy.get('#password').type("a_random_password")
        cy.get('[type=\'submit\']').should("not.have.attr", "disabled")
    })

    it('error submit email not found', () => {
        cy.get('#email').type("my@email.fr")
        cy.get('#password').type("a_random_password")
        cy.get('[type=\'submit\']').click()
        cy.get('#root > main > div > form > p').should("have.text", "There is no user record corresponding to this identifier. The user may have been deleted.")
    })

    it('error submit wrong password', () => {
        cy.get('#email').type("test@eco-surf.fr")
        cy.get('#password').type("a_random_password")
        cy.get('[type=\'submit\']').click()
        cy.get('#root > main > div > form > p').should("have.text", "The password is invalid or the user does not have a password.")
    })

    it('valid submit', () => {
        cy.get('#email').type("test@eco-surf.fr")
        cy.get('#password').type("password_test")
        cy.get('[type=\'submit\']').click()
        cy.wait(3000)
        cy.url().should('eq', 'http://localhost:8080/sessions')
        cy.get("#root > div > header > div > div").should("have.text", "Test Account")
    })
})