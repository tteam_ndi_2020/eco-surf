import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {Card,CardHeader} from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from '@material-ui/core/Grid';

import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import '../assets/css/SideDashboard.css';

import { Link, useHistory } from 'react-router-dom';
import AddSessionForm from './AddSessionForm';

import '../assets/css/SessionsPage.css';

import logo from '../assets/img/eco-surf-logo.svg';
import firebaseConnector from '../api/firebase-connector';
import { AccessAlarm } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: "200px",
    minHeight: "600px",
    borderRadius: '15px',
  },
}));

export default function MyFriendsDashboard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };




  return (
    <Card className={classes.root}>
      <CardHeader
        subheader={"Mes amis"}
      />
      
      {props.friendsNameList.map((text, index) => (
                  <CardHeader
                    avatar={
                      <Avatar src={props.friendsPicList[index]} key={text}/>
                    }
                    style={{textAlign:'left'}}
                    title={text}
                  />
        ))}
      
    </Card>
  );
}
