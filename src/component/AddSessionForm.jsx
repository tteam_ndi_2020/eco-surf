import React, { useEffect, useState } from 'react';
import GoogleMapReact from 'google-map-react';
import {
    Button,
    Card,
    CardContent,
    Container,
    Grid,
    TextField,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    FormLabel,
    Typography
} from '@material-ui/core';
import {
    SentimentVeryDissatisfiedSharp,
    SentimentVerySatisfiedSharp,
    SentimentSatisfiedSharp,
    SentimentDissatisfiedSharp,
    InsertEmoticonSharp,
    Description
} from '@material-ui/icons';
import firebaseConnector from '../api/firebase-connector';

import '../assets/css/AddSessionsForm.css'
import logo from "../assets/img/eco-surf-logo.svg";

export default function AddSessionForm(props){
    const [valid, setValid] = useState(false);
    const [noteProprete, setNoteProprete] = useState(3);
    const [time, setTime] = useState(null);
    const [trashNumber, setTrashNumber] = useState(null);
    const [ressenti, setRessenti] = useState(3);
    const [description, setDescription] = useState("Une super séance !");
    const [imageFile, setImageFile] = useState(null);
    const [Lat, setLat] = useState(null);
    const [Lng, setLng] = useState(null);
    const [Pays, setPays] = useState(null);

    const setNotePropreteCallback = (value) => {
        setNoteProprete(value);
        isValid();
    }

    const setTimeCallback = (value) => {
        setTime(value);
        isValid();
    }

    const setTrashNumberCallback = (value) => {
        setTrashNumber(value);
        isValid();
    }

    const setRessentiCallback = (value) => {
        setRessenti(value);
        isValid();
    }

    const setDescriptionCallback = (value) => {
        setDescription(value);
        isValid();
    }

    const setImageFileCallback = (value) => {
        setImageFile(value);
        isValid();
    }

    const setLatCallback = (value) => {
        setLat(value);
        isValid();
    }

    const setLngCallback = (value) => {
        setLng(value);
        isValid();
    }

    const setPaysCallback = (value) => {
        setPays(value);
        isValid();
    }

    const onSubmit = async (event) => {
        const dateOptions = { year: 'numeric', month: 'long', day: 'numeric', hour : '2-digit', minute : '2-digit' };

        const db = firebaseConnector.firestore();
        const collec = db.collection('sessions');
        const user_id = firebaseConnector.auth().currentUser.uid;
        const user_pseudo = firebaseConnector.auth().currentUser.displayName;
        const now = new Date(Date.now());
        const dataString = now.toLocaleDateString(undefined, dateOptions);
        const doc = await collec.add({
            user_pseudo: user_pseudo,
            pays: Pays,
            date: now,
            description: description,
            lat: Lat,
            lng: Lng,
            note_proprete: noteProprete,
            note_ressenti: ressenti,
            time: time,
            nb_dechet: trashNumber,
            user_id: user_id
        });

        await firebaseConnector.storage().ref().child("session-pictures").child(doc.id).put(imageFile);

        const downloadURL = await firebaseConnector.storage().ref().child("session-pictures").child(doc.id).getDownloadURL()

        const res = await doc.set({
            picture_link: downloadURL
        }, { merge: true });

        props.afterSubmitCallback();

    };

    const isValid = (event) => {

        console.log(noteProprete == null);
        console.log(time == null);
        console.log(ressenti == null);
        console.log(description == null);
        console.log(Lat == null);
        console.log(Lng == null);
        console.log(Pays == null);
        console.log(imageFile == null);
        setValid(
            noteProprete == null ||
            time == null ||
            ressenti == null ||
            description == null ||
            Lat == null ||
            Lng == null ||
            Pays == null ||
            imageFile == null
        )
    };

    useEffect(()=>{
        isValid();
    })

    return (
        <Container maxWidth="md" style={{textAlign: "center", overflow : 'scroll', backgroundColor:'white', marginTop:'50px', maxHeight : '90%', padding : '30px 50px'}} >
            <Typography variant="h3">
                Ajouter une session
            </Typography>
            <NoteProprete callback={setNotePropreteCallback}/>
            <TrashNumber callback={setTrashNumberCallback}/>
            <SessionTime callback={setTimeCallback}/>
            <Ressenti callback={setRessentiCallback}/>
            <SessionDescription callback={setDescriptionCallback}/>
            <PhotoSelect callback={setImageFileCallback}/>
            <MapSelect callbackLat={setLat} callbackLng={setLngCallback} callbackPays={setPaysCallback}/>
            <Button variant="outlined" onClick={onSubmit} color="primary" disabled={valid}>
                Valider
            </Button>
        </Container>
    )
}


function NoteProprete(props) {
    const [selectedValue, setSelectedValue] = React.useState('3');

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
        props.callback(event.target.value);
    };

    return (
        <div class="question">
            <FormControl component="fieldset">
                <FormLabel component="legend">Note de propreté du spot :</FormLabel><br/>
                <RadioGroup row aria-label="position" name="position" defaultValue="1">
                    <FormControlLabel
                        value="1"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '1'}
                                onChange={handleChange}
                            />
                        }
                        label="1"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="2"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '2'}
                                onChange={handleChange}
                            />
                        }
                        label="2"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="3"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '3'}
                                onChange={handleChange}
                            />
                        }
                        label="3"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="4"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '4'}
                                onChange={handleChange}
                            />
                        }
                        label="4"
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="5"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '5'}
                                onChange={handleChange}
                            />
                        }
                        label="5"
                        labelPlacement="top"
                    />
                </RadioGroup>
            </FormControl>
        </div>
    );
}

function SessionTime(props) {

    const handleChange = (event) => {
        props.callback(event.target.value);
    };

    return (
        <div class="question">
            <TextField label="Temps en minutes" type="number" onChange={handleChange} variant="outlined" />
        </div>
    );
}

function TrashNumber(props) {

    const handleChange = (event) => {
        props.callback(event.target.value);
    };

    return (
        <div class="question">
            <TextField label="Déchets ramassés" type="number" onChange={handleChange} variant="outlined" />
        </div>
    );
}

function Ressenti(props) {
    const [selectedValue, setSelectedValue] = React.useState('3');

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
        props.callback(event.target.value);
    };

    return (
        <div class="question">
            <FormControl component="fieldset">
                <FormLabel component="legend">Comment vous sentiez vous pendant cette séance :</FormLabel><br/>
                <RadioGroup row aria-label="position" name="position" defaultValue="1">
                    <FormControlLabel
                        value="1"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '1'}
                                onChange={handleChange}
                            />
                        }
                        label={<SentimentVeryDissatisfiedSharp/>}
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="2"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '2'}
                                onChange={handleChange}
                            />
                        }
                        label={<SentimentDissatisfiedSharp/>}
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="3"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '3'}
                                onChange={handleChange}
                            />
                        }
                        label={<SentimentSatisfiedSharp/>}
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="4"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '4'}
                                onChange={handleChange}
                            />
                        }
                        label={<InsertEmoticonSharp/>}
                        labelPlacement="top"
                    />
                    <FormControlLabel
                        value="5"
                        control={
                            <Radio
                                color="primary"
                                checked={selectedValue === '5'}
                                onChange={handleChange}
                            />
                        }
                        label={<SentimentVerySatisfiedSharp/>}
                        labelPlacement="top"
                    />
                </RadioGroup>
            </FormControl>
        </div>
    );
}

function SessionDescription(props) {

    const handleChange = (event) => {
        props.callback(event.target.value);
    };

    return (
        <div class="question">
            <TextField
                id="outlined-multiline-static"
                label="Description"
                multiline
                rows={4}
                onChange={handleChange}
                defaultValue="Une super séance !"
                variant="outlined"
                fullWidth="true"
            />
        </div>
    );
}

function PhotoSelect(props) {
    const [file, setFile] = useState(null);
    const [url, setURL] = useState("");

    const handleClick = event => {
        document.querySelector("#contained-button-file").click();
    };

    const handleChange = (e) => {
        setFile(e.target.files[0]);
    };

    useEffect(()=>{
        props.callback(file);
    })

    return (
        <div class="question">
            <Typography id="file-name">{(file != null ) ? file.name : "Pas d'image"}</Typography>
            <input
                accept="image/*"
                className="fileInput"
                onChange={handleChange}
                id="contained-button-file"
                multiple
                type="file"
            />
            <label htmlFor="contained-button-file">
                <Button variant="outlined" color="secondary" onClick={handleClick}>
                    Parcourir
                </Button>
            </label>
        </div>
    );
}

const Marker = props => {
    let marker = null;
    const markerStyle = {
        border: '1px solid white',
        backgroundColor : 'blue',
        borderRadius: '50%',
        height: 10,
        width: 10,
        cursor: 'pointer',
        zIndex: 10,
    };

    useEffect(()=>{
        if(marker != null) {
            marker.setMap(null);
        }
    })

    if(props.maps.Marker == undefined){
        return (<div></div>);
    }
    var map = props.map;
    marker = new props.maps.Marker({
            position: { lat: props.lat, lng: props.lng },
            map,
            title: 'Hello World!'
    });

    return (
        <>
            <div style={markerStyle} />

        </>
    );
};
function MapSelect(props) {
    const [lat, setLat] = useState(47.36469576840396);
    const [lng, setLng] = useState(0.6844627305156692);
    const [pays, setPays] = useState(null);
    const [mapSave, setMapSave] = useState(0.6844627305156692);
    const [mapsSave, setMapsSave] = useState(0.6844627305156692);

    const handleClickOnMap = ({x, y, lat, lng, event}) => {
        setLat(lat);
        setLng(lng);
        props.callbackLat(lat);
        props.callbackLng(lng);
    };

    const renderMarkers = (map, maps) => {
        setMapSave(map);
        setMapsSave(maps);

        const latlng = {
            lat: lat,
            lng: lng,
        };
        const geocoder = new maps.Geocoder();
        geocoder.geocode({location: latlng}, (result, status) => {
            if (status === "OK") {
                if (result[9]) {
                    var tab = result[9].formatted_address.split(", ");
                    setPays(tab[tab.length -1]);
                    props.callbackPays(tab[tab.length -1]);
                } else {
                    window.alert("Vous etes hors limites !");
                }
            } else {
                window.alert("Geocoder failed due to: " + status);
            }
        })
    };

    return (
        // Important! Always set the container height explicitly
        <div style={{width: '100%', margin : '20px 0' }}>
            <Typography>Cliquez à l'emplacement de votre session</Typography>
            <Typography>Vous êtes en {pays}</Typography>
            <div style={{ height: '400px', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyDF3QAgGTFVsWnb4nsg1q7xJBLzAxcfqFA" }}
                    defaultCenter={{
                        lat: 47.36469576840396,
                        lng: 0.6844627305156692
                    }}
                    defaultZoom={15}
                    onClick={handleClickOnMap}
                    onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
                >
                    <Marker maps={mapsSave} map={mapSave} lng={lng} lat={lat}/>
                </GoogleMapReact>
            </div>
        </div>
    );
}