import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {Card,CardHeader} from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from '@material-ui/core/Grid';

import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import '../assets/css/SideDashboard.css';

import { Link, useHistory } from 'react-router-dom';
import AddSessionForm from './AddSessionForm';

import '../assets/css/SessionsPage.css';

import logo from '../assets/img/eco-surf-logo.svg';
import firebaseConnector from '../api/firebase-connector';
import { AccessAlarm } from '@material-ui/icons';

export default function EcoCoinIndicator(props) {

    const [nbCoins, setNbCoins] = React.useState(0);

    useEffect(async ()=> {
        var totalCoin = 0;
        const user_id = props.user_id;
        const db = firebaseConnector.firestore();
        const snapshot = await db.collection('sessions').where("user_id", "==", user_id).get();
        if (!snapshot.empty) {
            snapshot.forEach(doc => {
                totalCoin += parseInt(doc.data().nb_dechet);
            });
        }
        setNbCoins(totalCoin);
    });

    if(props.small){
        return (
            <div style={{margin:"5px"}}>
                <p style={{color: "white", backgroundColor:'#0c0', borderRadius:"25px", fontWeight:"800", padding:'5px 10px'}}>{nbCoins}</p>
            </div>
        );
    }else{
        return (
            <div style={{margin:"5px 25px"}}>
                <Typography style={{color: "#0c0", fontWeight:"800"}}>{nbCoins} écoCoins</Typography>
            </div>
        );
    }
}
