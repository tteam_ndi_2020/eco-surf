import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import firebaseConnector from "../api/firebase-connector";
import {Container, TextField, Typography} from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  button: {
    marginBottom: '10px',
    borderRadius: '15px',
    backgroundColor:"#FFFFFF"
  },
}));

export default function AddFriendsForm(props) {
    const [valid, setValid] = useState(false);
    const [pseudo, setPseudo] = useState("");
    const [id, setId] = useState("");
    const [picLink, setPicLink] = useState("");

    const onSubmit = async (event) => {

        const db = firebaseConnector.firestore();
        const user_id = firebaseConnector.auth().currentUser.uid;
        await db.collection('users_followers').doc(user_id).collection('followers').doc(id).set({
            user_id: id,
            user_name: pseudo,
            user_picture_link : picLink
        });

        props.afterSubmitCallback();

    };

    const handleChange = (event) => {
        setPseudo(event.target.value);
        isValid();
    };

    const isValid = async (event) => {
        const snapshot = await firebaseConnector.firestore().collection('users_followers').where('user_name', '==', pseudo).get();

        if (!snapshot.empty) {
            snapshot.forEach(doc => {
                setId(doc.data().user_id);
                setPicLink(doc.data().user_picture_link);
            });
        }

        setValid(snapshot.empty);
    };

    useEffect(()=>{
        isValid();
    })

    return (
        <Container maxWidth="md" style={{textAlign: "center", overflow : 'scroll', backgroundColor:'white', marginTop:'50px', maxHeight : '90%', padding : '30px 50px'}} >
            <Typography variant="h3">
                Ajouter un ami
            </Typography>
            <TextField label="Pseudo" type="text" onChange={handleChange} variant="outlined" />
            <Button variant="outlined" onClick={onSubmit} color="primary" disabled={valid}>
                Valider
            </Button>
        </Container>
    )
}