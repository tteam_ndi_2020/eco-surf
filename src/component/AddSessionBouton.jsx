import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
  button: {
    marginBottom: '10px',
    borderRadius: '15px',
    backgroundColor:"#FFFFFF"
  },
}));

export default function AddSessionBouton(props) {
  const classes = useStyles();

    const handleClick = (event) => {
        props.openFunction();
    };

  return (
    <Grid container justify="center" alignItems="center">
      <Button
        variant="contained"
        size="large"
        className={classes.button}
        onClick={handleClick}
        startIcon={<AddIcon color="primary" />}
      >
        | Ajouter une session
      </Button>
    </Grid>
  );
}