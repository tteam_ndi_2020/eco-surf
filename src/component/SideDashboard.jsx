import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {Card,CardHeader} from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from '@material-ui/core/Grid';

import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import '../assets/css/SideDashboard.css';

import { Link, useHistory } from 'react-router-dom';
import AddSessionForm from './AddSessionForm';

import '../assets/css/SessionsPage.css';

import logo from '../assets/img/eco-surf-logo.svg';
import firebaseConnector from '../api/firebase-connector';
import { AccessAlarm } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: "200px",
    minHeight: "600px",
    borderRadius: '15px',
  },
}));

export default function SideDashboard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  var monthTrashes = 0;
  var monthSessions = 0;
  var nbSessions = 0;
  var sumCleanlinessSessions = 0;


  props.listSessionsCard.map((feedDoc, index) => {
    const feedData = feedDoc.data();
    var date = new Date(feedData.date.seconds * 1000);
    var now = new Date()
    if(date.getMonth() == now.getMonth() && date.getYear() == now.getYear()){
        monthTrashes +=1
    }
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    date = date.toLocaleDateString(undefined, options);
    monthSessions+=1;
    sumCleanlinessSessions = sumCleanlinessSessions + parseInt(feedData.note_proprete);
    nbSessions +=1;
    console.log("nb sessions : " + nbSessions)
    console.log("sumCleanlinessSessions : " + sumCleanlinessSessions)
  }
  )

  return (
    <Card className={classes.root}>
      <Grid container justify="center" alignItems="center">
        <Grid item>
          <CardHeader
            title="Objectif sessions Mensuels"
          />
        </Grid>
        <Grid container justify="center" alignItems="center">
          <CircularProgressbar
          value={monthSessions*20}
          text={monthSessions*20 + "%"}
          />
        </Grid>
        <Grid item>
          <CardHeader
          title="Objectif déchets Mensuels"
          />
        </Grid>
        <Grid container justify="center" alignItems="center">
          <CircularProgressbar
          value={monthTrashes}
          text={monthTrashes + "%"}
          />
        </Grid>
        <Grid item>
          <CardHeader
          title="Propreté des plages visitées"
          />
        </Grid>
        <Grid container justify="center" alignItems="center">
          <CircularProgressbar
          value={(sumCleanlinessSessions/nbSessions)*20}
          text={(sumCleanlinessSessions/nbSessions)*20 + "%"}
          />
        </Grid>
      </Grid>
    </Card>
  );
}
