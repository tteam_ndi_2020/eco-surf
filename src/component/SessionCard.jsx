import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {Card,CardHeader} from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from '@material-ui/core/Grid';

import { Link, useHistory } from 'react-router-dom';
import AddSessionForm from '../component/AddSessionForm';
import EcoCoinIndicator from './EcoCoinIndicator';


import logo from '../assets/img/eco-surf-logo.svg';
import firebaseConnector from '../api/firebase-connector';
import { AccessAlarm } from '@material-ui/icons';
import GoogleMapReact from "google-map-react";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: "700px",
    maxWidth: "1000px",
    borderRadius: '15px',
    marginBottom : '15px'
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function SessionCard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [picture_link, setPicture_link] = React.useState("");

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };


  useEffect(async ()=> {
    const user_id = props.user_id;
    const db = firebaseConnector.firestore();
    const doc = await db.collection('users_followers').doc(user_id).get();
    if(doc.exists){
      setPicture_link(doc.data().user_picture_link);
    }
  });

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <div style={{display:'flex', alignItems:'center'}}>
            <Avatar src={picture_link}/>
            <EcoCoinIndicator user_id={props.user_id} small/>
          </div>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={props.pseudo}
        subheader={props.date}
      />
      <Grid container>
        <Grid item xs={6}>
        <CardMedia
            className={classes.media}
            image={props.image1Path}
            title="Paella dish"
        />
        </Grid>
        <Grid item xs={6}>
          <MapCard lat={props.lat} lng={props.lng}/>
        </Grid>
        
        </Grid>
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            {props.description}
        </Typography>
      </CardContent>
      <Grid container spacing={3} style={{marginLeft : "30px"}}>
        <Grid item xs={4}>
             <p><FavoriteIcon color="secondary"/>{props.cleanliness}/5 propreté</p>
        </Grid>
        <Grid item xs={4}>
             <p><DeleteIcon color="primary"/>{props.numberTrashes} dechets ramassés</p>
        </Grid>
        <Grid item xs={4}>
            <p><AccessAlarm  color="primary"/>{props.hours} heure {props.minutes} minutes</p>
        </Grid>
    </Grid>
    </Card>
  );
}

function MapCard(props) {

  const renderMarkers = (map, maps) => {
    let marker = new maps.Marker({
      position: { lat: props.lat, lng: props.lng },
      map,
      title: 'Hello World!'
    });
    return marker;
  };

  return (
      <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyDF3QAgGTFVsWnb4nsg1q7xJBLzAxcfqFA" }}
          defaultCenter={{lat: props.lat, lng: props.lng}}
          defaultZoom={15}
          onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
      >
      </GoogleMapReact>
  );
}
