import React, { useEffect, useState } from 'react';
import { Card, Container, FormControl, InputLabel, MenuItem, Select, Typography } from '@material-ui/core';
import firebaseConnector from '../api/firebase-connector';
import { Line } from 'react-chartjs-2';
import Grid from '@material-ui/core/Grid';
import GoogleMapReact from "google-map-react";
import { RadioButtonUnchecked } from '@material-ui/icons';
import CardContent from '@material-ui/core/CardContent';

export default function StatsSection(){
    return(
        <section>
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <StatsChartLine/>
                </Grid>
                <Grid item xs={4}>
                    <StatsSumWaste/>
                </Grid>
                <Grid item xs={8}>
                    <MapData/>
                </Grid>
                <Grid item xs={4}>
                    <StatsAverageNote/>
                </Grid>
            </Grid>
        </section>
    )
}

const options = {
    scales: {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                },
            },
        ],
    },
}

function StatsChartLine(){
    const [datasets, setDatasets] = useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

    useEffect(()=>{
        let query = firebaseConnector.firestore().collection("sessions");

        query.onSnapshot((snapshot => {
            let data = []
            datasets.forEach((dataMonth, index)=>{
                const docsList = snapshot.docs.filter(doc=>{
                    let session=doc.data();
                    return (index === new Date(session.date.seconds * 1000).getMonth())
                });
                data[index] = docsList.length
            })
            setDatasets(data)
        }))
    }, [])

    let data = {
        labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        datasets: [{
            label: 'Nombre de sessions',
            data: datasets,
            fill: false,
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgba(255, 99, 132, 0.2)',
        }],
    }
    return  <Line data={data} options={options} />
}

function StatsSumWaste(){
    const [sum, setSum] = useState(0)

    useEffect(()=>{
        let query = firebaseConnector.firestore().collection("sessions");
        query.onSnapshot((snapshot => {
            let sum = 0;
            snapshot.forEach((doc)=>{
                const data = doc.data()
                sum+=Number(data.nb_dechet);
            })
            setSum(sum)
        }))
    }, [])

    return(
        <Card>
            <CardContent>
                <Typography variant="h5" component="h2">
                    <h3>Nombre de déchets récoltés: {sum}</h3>
                </Typography>
            </CardContent>
        </Card>
    )
}

function StatsAverageNote(){
    const [averages, setAverages] = useState([0, 0, 0, 0])

    useEffect(()=>{
        let query = firebaseConnector.firestore().collection("sessions");
        query.onSnapshot((snapshot => {
            let nb = snapshot.docs.length;
            let sum = [0,0,0,0,0]
            snapshot.forEach((doc)=>{
                const data = doc.data();
                sum[Number(data.note_ressenti)] += 1
            })
            setAverages([Math.trunc((sum[0]/nb)*100), Math.trunc((sum[1]/nb)*100), Math.trunc((sum[2]/nb)*100), Math.trunc(((sum[3]+sum[4])/nb)*100)])
        }))
    }, [])

    return(
        <div style={{backgroundColor: "#F5F6FB", borderRadius: "20px", marginTop: "1rem"}}>
            <Container style={{paddingTop: "1rem", paddingBottom: "1rem"}}>
                <h3>Moyenne des notes ressenties : </h3>
                <Grid container spacing={3}>
                    <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                        <RadioButtonUnchecked style={{color: "#52AA43", marginRight: "1rem"}}/><h4>Bonne : {averages[3]}%</h4>
                    </Grid>
                    <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                        <RadioButtonUnchecked style={{color: "#FFB84F", marginRight: "1rem"}}/><h4>Moyenne : {averages[2]}%</h4>
                    </Grid>
                    <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                        <RadioButtonUnchecked style={{color: "#3741B4", marginRight: "1rem"}}/><h4>Bof : {averages[1]}%</h4>
                    </Grid>
                    <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                        <RadioButtonUnchecked style={{color: "#FE895E", marginRight: "1rem"}}/><h4>Nul : {averages[0]}%</h4>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

const Marker = props => {
    let marker = null;

    const nbDechet = props.nbDechet;
    var r = (nbDechet * 8);
    if(r > 255){
        r = 255;
    }
    var g = (255 - (nbDechet * 8));
    if(g < 0){
        g = 0;
    }

    console.log(nbDechet + " ->" + r + " - " + g);

    const markerStyle = {
        border: '1px solid white',
        backgroundColor : `rgb(${r}, ${g}, 0)`,
        borderRadius: '50%',
        height: 10,
        width: 10,
        cursor: 'pointer',
        zIndex: 10,
    };

    useEffect(()=>{
        if(marker != null) {
            marker.setMap(null);
        }
    })

    if(props.maps.Marker == undefined){
        return (<div></div>);
    }
    var map = props.map;
    marker = new props.maps.Marker({
        position: { lat: props.lat, lng: props.lng },
        map,
        title: 'Hello World!'
    });

    return (
        <>
            <div style={markerStyle} />
        </>
    );
};
function MapData(props) {
    const [mapSave, setMapSave] = useState(0.6844627305156692);
    const [mapsSave, setMapsSave] = useState(0.6844627305156692);
    const [listMarkers, setListMarkers] = useState([]);

    const datas = firebaseConnector.firestore().collection('sessions').onSnapshot(
        (snapshot) => {
            setListMarkers(snapshot.docs)
        }
    );

    const renderMarkers = (map, maps) => {
        setMapSave(map);
        setMapsSave(maps);
    };

    return (
        // Important! Always set the container height explicitly
        <div style={{ width: '100%', margin: '20px 0' }}>
            <Typography>Cliquez à l'emplacement de votre session</Typography>
            <div style={{ height: '400px', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyDF3QAgGTFVsWnb4nsg1q7xJBLzAxcfqFA" }}
                    defaultCenter={{
                        lat: 47.36469576840396,
                        lng: 0.6844627305156692
                    }}
                    defaultZoom={15}
                    onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
                >
                    {listMarkers.map((feedDoc, index) =>
                        (<Marker maps={mapsSave} map={mapSave} lng={feedDoc.data().lng} lat={feedDoc.data().lat}
                                 nbDechet={feedDoc.data().nb_dechet} />)
                    )}

                </GoogleMapReact>
            </div>
        </div>
    );
}
