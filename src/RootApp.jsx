import React from 'react';
import {Switch, Route, useHistory, useLocation, Redirect} from 'react-router-dom'

import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import { DonutLargeOutlined, HomeOutlined, ViewDay} from '@material-ui/icons';
import { AppBar, Container, IconButton, Toolbar, Typography, Avatar } from '@material-ui/core';

import AuthRoute from './component/AuthRoute';
import EcoCoinIndicator from './component/EcoCoinIndicator';

import SessionsPage from './page/SessionsPage';
import FeedPage from './page/FeedPage';
import LoginPage from './page/LoginPage';
import logo from './assets/img/eco-surf-logo.svg';

import routes_config from './route/route.config.json';

import firebaseConnector from './api/firebase-connector';
import SignUpPage from './page/SignUpPage';
import StatsSection from './section/StatsSection';


export default function RootApp(){
    return(
        <Switch>
            <Route path='/signup' exact component={SignUpPage} />
            <Route path='/login' exact component={LoginPage} />
            <AuthRoute path='/' component={App} />
        </Switch>
    )
}

function App(){
    const drawerWidth = 300;

    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
        },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0,
            },
        },
        appBar: {
            backgroundColor: "white",
            color: "#0063A8",
            [theme.breakpoints.up('sm')]: {
                boxShadow: "none",
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
            [theme.breakpoints.down('md')]: {
                boxShadow: "0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)",
                backgroundColor: "#0063A8",
                color: "white",
            },
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
        // necessary for content to be below app bar
        toolbar: theme.mixins.toolbar,
        toolbarTitle:{
            flexGrow: "1",
            [theme.breakpoints.up('md')]: {
                marginTop: "1rem",
                fontWeight: "bold",
                fontSize: "1.5rem",
            },
            [theme.breakpoints.down('sm')]: {
                fontSize: "1.2rem"
            },
        },
        drawerPaper: {
            width: drawerWidth,
            backgroundColor: "#FBFBFF",
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            margin : 75,
        },
        drawerImg: {
            [theme.breakpoints.up('md')]: {
                width: "8vw",
            },
            [theme.breakpoints.down('md')]: {
                width: "35vw",
            },
        },
        drawerItem: {
            marginBottom: "1rem",
            paddingLeft: "2rem",
            paddingTop: "1rem",
            paddingBottom: "1rem",
        },
        drawerItemActive: {
            marginBottom: "1rem",
            paddingLeft: "2rem",
            color: "#0063A8",
            backgroundColor: "rgba(13, 106, 172, 0.1)!important",
            paddingTop: "1rem",
            paddingBottom: "1rem",
        },
        drawerItemIconActive:{
            color: "#0063A8",
        },
        drawerItemIcon:{
            color: "#898CAA"
        },
        logoutLink:{
            bottom: "3vh",
            marginLeft: "6rem",
            position: "fixed",
            fontSize: "1.2rem",
        }
    }));

    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    let history = useHistory();
    let location = useLocation();

    console.log(location.pathname)
    console.log(routes_config)

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <div className={classes.toolbar} style={{textAlign: "center", marginTop: "2rem", marginBottom: "1.5rem",}}>
                <img src={logo} alt="logo-eco-surf" className={classes.drawerImg}/>
            </div>
            <List style={{color: "#898CAA"}}>
                <ListItem
                    button
                    onClick={()=>{history.push("/feed")}}
                    alignItems={'center'}
                    selected={location.pathname === "/feed"}
                    className={location.pathname === "/feed" ? classes.drawerItemActive : classes.drawerItem}
                >
                    <ListItemIcon>
                        <HomeOutlined fontSize="large" className={location.pathname === "/feed" ? classes.drawerItemIconActive : classes.drawerItemIcon}/>
                    </ListItemIcon>
                    <ListItemText primary={"File d'actualité"} style={{fontSize: "1.4rem"}} disableTypography={true}/>
                </ListItem>
                <ListItem
                    button
                    onClick={()=>{history.push("/sessions")}}
                    alignItems={'center'}
                    selected={location.pathname === "/sessions"}
                    className={location.pathname === "/sessions" ? classes.drawerItemActive : classes.drawerItem}
                >
                    <ListItemIcon>
                        <ViewDay fontSize="large" className={location.pathname === "/sessions" ? classes.drawerItemIconActive : classes.drawerItemIcon}/>
                    </ListItemIcon>
                    <ListItemText primary="Mes sessions" style={{fontSize: "1.4rem"}} disableTypography={true}/>
                </ListItem>
                <ListItem
                    button
                    onClick={()=>{history.push("/stats")}}
                    alignItems={'center'}
                    selected={location.pathname === "/stats"}
                    className={location.pathname === "/stats" ? classes.drawerItemActive : classes.drawerItem}
                >
                    <ListItemIcon>
                        <DonutLargeOutlined fontSize="large" className={location.pathname === "/stats" ? classes.drawerItemIconActive : classes.drawerItemIcon}/>
                    </ListItemIcon>
                    <ListItemText primary="Statistiques" style={{fontSize: "1.4rem"}} disableTypography={true}/>
                </ListItem>
            </List>
            <a href="#" className={classes.logoutLink} onClick={()=>{firebaseConnector.auth().signOut()}}>
                Deconnexion
            </a>
        </div>
    );

    const container = window.document.body

    return (
        <div className={classes.root}>
            <AppBar position="absolute" className={classes.appBar}>
                <Toolbar style={{paddingTop: "1rem"}}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h4" className={classes.toolbarTitle} noWrap>
                        {routes_config.hasOwnProperty(location.pathname) ? routes_config[location.pathname] : ""}
                    </Typography>
                    <div style={{marginRight: "15rem", display: "contents", color: "#0E1566", marginTop: "1rem"}}>
                        <EcoCoinIndicator user_id={firebaseConnector.auth().currentUser.uid}/>
                        <Avatar src={firebaseConnector.auth().currentUser.photoURL} alt="userconnected-img" style={{marginRight: "1rem"}}/>
                        <Typography variant="h6" style={{marginRight: "2.5rem"}}>{firebaseConnector.auth().currentUser.displayName}</Typography>
                    </div>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{ paper: classes.drawerPaper, }}
                        ModalProps={{ keepMounted: true, }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{ paper: classes.drawerPaper, }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <Switch>
                    <Route path='/' exact><Redirect to="/sessions"/></Route>

                    <Route path='/sessions' exact component={SessionsPage} />
                    <Route path='/feed' exact component={FeedPage} />
                    <Route path='/stats' exact component={StatsSection} />
                    <Route>404</Route>
                </Switch>
            </main>
        </div>
    )
}