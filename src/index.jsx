import React from 'react';
import ReactDOM from 'react-dom';
import { CssBaseline } from '@material-ui/core';
import {BrowserRouter, useHistory} from 'react-router-dom';
import Swal from 'sweetalert2';

import './index.css';
import RootApp from './RootApp';
import firebaseConnector from './api/firebase-connector';
import ToastContext from "./context/ToastContext";

let firebase_auth_checked = false

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

firebaseConnector.auth().onAuthStateChanged(()=>{
    if(!firebase_auth_checked) {
        firebase_auth_checked = true
        ReactDOM.render(
            <BrowserRouter>
                <CssBaseline />
                <ToastContext.Provider value={{toast: toast}}>
                    <RootApp />
                </ToastContext.Provider>
            </BrowserRouter>,
            document.getElementById('root')
        );
    }
})

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://www.snowpack.dev/#hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept();
}