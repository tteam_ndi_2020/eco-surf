import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAkAT32GDx3SSOdDhadG-Upm7YxHFekeq8",
  authDomain: "n2i-eco-surf.firebaseapp.com",
  projectId: "n2i-eco-surf",
  storageBucket: "n2i-eco-surf.appspot.com",
  messagingSenderId: "272411340808",
  appId: "1:272411340808:web:6b08c8945ab6ca23d6a1bd"
};

try {
  firebase.initializeApp(firebaseConfig);
  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
} catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)}
}

const firebaseConnector = firebase;
export default firebaseConnector;