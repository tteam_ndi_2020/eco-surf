import React, { useEffect, useState } from 'react';
import {Button, Card, CardContent, Container, Grid, Modal, TextField} from '@material-ui/core';

import { Link, useHistory} from 'react-router-dom';
import SessionCard from '../component/SessionCard';

import '../assets/css/SessionsPage.css'
import logo from '../assets/img/eco-surf-logo.svg'
import firebaseConnector from '../api/firebase-connector';
import MyFriendsDashboard from '../component/MyFriendsDashboard';
import AddFriendsForm from '../component/AddFriendsForm';
import AddFriendBouton from "../component/AddFriendBouton";

export default function FeedPage(){

    const [open, setOpen] = React.useState(false);
    const [listSessionsCard, setListSessionsCard] = useState([]);
    const [tabFollowPic, setTabFollowPic] = useState([]);
    const [tabFollowName, setTabFollowName] = useState([]);
    var tempTabFollowPic = [];
    var tempTabFollowName = [];
    var tempTabFollowId = [];

    const rootRef = React.useRef(null);


    useEffect(async ()=>{

        const user_id = firebaseConnector.auth().currentUser.uid;
        const db = firebaseConnector.firestore();
        const allFollow = await db.collection('users_followers').doc(user_id).collection('followers').get();

        allFollow.forEach(doc => {
            console.log(doc.id, '=>', doc.data());
            tempTabFollowName.push(doc.data().user_name);
            tempTabFollowPic.push(doc.data().user_picture_link);
            tempTabFollowId.push(doc.data().user_id);
        });

        setTabFollowPic(tempTabFollowPic);
        setTabFollowName(tempTabFollowName);
        console.log(tempTabFollowId);

        firebaseConnector.firestore().collection('sessions').where('user_id', 'in', tempTabFollowId).orderBy('date', 'desc').limit(50).onSnapshot(
            (snapshot)=>{
                setListSessionsCard(snapshot.docs)
            }
        );

    },[])

    return (
        <Container maxWidth="lg" style={{textAlign: "center"}} className="mobile-container"  ref={rootRef}>
            <Modal
                disablePortal
                disableEnforceFocus
                disableAutoFocus
                aria-labelledby="server-modal-title"
                aria-describedby="server-modal-description"
                container={() => rootRef.current}
                onClose={event => setOpen(false)}
                open={open}
                id="addSectionModal"
            >
                <AddFriendsForm afterSubmitCallback={() => setOpen(false)}/>
            </Modal>
            <Grid container spacing={3}>
                <Grid item md={8} spacing={3}>
                    <div style={{paddingTop : '51px'}}>
                        {listSessionsCard.map((feedDoc, index) =>
                            (<FeedCard feedDoc={feedDoc} key={index}/>)
                        )}
                    </div>
                </Grid>

                <Grid item md={4} spacing={3} style={{
        position: '-webkit-sticky',
        position: 'sticky',}}>
                    <Grid item>
                        <AddFriendBouton openFunction={() => {
                            setOpen(true);

                        }}/>
                    </Grid>
                    <Grid item>
                        <MyFriendsDashboard friendsNameList={tabFollowName} friendsPicList={tabFollowPic}/>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}

function FeedCard({feedDoc}) {
    const feedData = feedDoc.data();
    var date = new Date(feedData.date.seconds * 1000);
    var now = new Date()
    const options = { year: 'numeric', month: 'long', day: 'numeric', hour : '2-digit', minute : '2-digit' };
    date = date.toLocaleDateString(undefined, options);
    return (
            <Grid item>
                <SessionCard
                    pseudo={feedData.user_pseudo}
                    user_id={feedData.user_id}
                    date={date}
                    cleanliness={feedData.note_proprete}
                    numberTrashes={feedData.nb_dechet}
                    hours={Math.floor(feedData.time/60)}
                    minutes={feedData.time%60}
                    description={feedData.description}
                    image1Path={feedData.picture_link}
                    lat={feedData.lat}
                    lng={feedData.lng}
                />
            </Grid>
    )
}