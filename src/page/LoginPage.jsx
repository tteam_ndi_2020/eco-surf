import React, { useContext, useEffect, useState } from 'react';
import { Button, Container, Grid, makeStyles, TextField, Typography } from '@material-ui/core';

import { Link, useHistory} from 'react-router-dom';

import '../assets/css/login.css'
import logo from '../assets/img/eco-surf-logo.svg'
import firebaseConnector from '../api/firebase-connector';
import ToastContext from '../context/ToastContext';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        width: "20vw"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function LoginPage(){
    let history = useHistory();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")
    const [isFormValid, setIsFormValid] = useState(false)
    const {toast} = useContext(ToastContext);

    useEffect(()=>{
        setIsFormValid(email !== "" && password !== "")
    })

    const onSubmit = event => {
        event.preventDefault();

        firebaseConnector.auth().signInWithEmailAndPassword(email, password)
            .then(authUser => {
                toast.fire({
                    icon: "success",
                    title: "Connecté"
                })
                history.push(`/`)
            })
            .catch(error => {
                setError(error)
            });
    };

    const classes = useStyles();

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <img src={logo} alt="logo" className={classes.avatar}/>
                <Typography component="h1" variant="h5">
                    Connexion
                </Typography>
                <form className={classes.form} noValidate onSubmit={onSubmit}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        onChange={(event)=>{setEmail(event.target.value)}}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        onChange={(event)=>{setPassword(event.target.value)}}
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={!isFormValid}
                    >
                        Sign In
                    </Button>
                    {error && <p style={{color: "red"}}>{error.message}</p>}
                    <Grid container>
                        <Grid item xs>
                            <Link to="/reset-password" variant="body2">
                                Forgot password?
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link to="/signup" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}