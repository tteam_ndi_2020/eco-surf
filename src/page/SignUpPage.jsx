import React, { useContext, useEffect, useState } from 'react';
import { Button, Container, Grid, makeStyles, TextField, Typography } from '@material-ui/core';

import { Link, useHistory} from 'react-router-dom';

import '../assets/css/login.css'
import logo from '../assets/img/eco-surf-logo.svg'
import firebaseConnector from '../api/firebase-connector';
import ToastContext from '../context/ToastContext';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        width: "20vw"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignUpPage(){
    let history = useHistory();

    const [pseudo, setPseudo] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [repassword, setRepassword] = useState("")
    const [file, setFile] = useState({})
    const [error, setError] = useState("")
    const [isFormValid, setIsFormValid] = useState(false)
    const {toast} = useContext(ToastContext);

    useEffect(()=>{
        setIsFormValid(email !== "" && password !== "" && repassword !== "" && pseudo !== "" && file !== {} && (password === repassword))
    })

    const onSubmit = event => {
        event.preventDefault();

        firebaseConnector.auth().createUserWithEmailAndPassword(email, password)
            .then(async authUser => {
                await firebaseConnector.storage().ref().child("user-avatar").child(authUser.user.uid).put(file)
                const downloadURL = await firebaseConnector.storage().ref().child("user-avatar").child(authUser.user.uid).getDownloadURL()
                await authUser.user.updateProfile({
                    displayName: pseudo,
                    photoURL: downloadURL
                })
                const db = firebaseConnector.firestore();
                const collec = db.collection('users_followers');
                await collec.doc(authUser.user.uid).set({
                    user_id: authUser.user.uid,
                    user_name: authUser.user.displayName,
                    user_picture_link : downloadURL
                });
                await collec.doc(authUser.user.uid).collection('followers').doc(authUser.user.uid).set({
                    user_id: authUser.user.uid,
                    user_name : authUser.user.displayName,
                    user_picture_link : downloadURL
                });
                toast.fire({
                    icon: "success",
                    title: "Compte créé"
                })
                history.push(`/`)
            })
            .catch(error => {
                setError(error)
            });
    };

    const classes = useStyles();

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <img src={logo} alt="logo" className={classes.avatar}/>
                <Typography component="h1" variant="h5">
                    Inscription
                </Typography>
                <form className={classes.form} noValidate onSubmit={onSubmit}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="pseudo"
                        label="Pseudo"
                        name="pseudo"
                        autoComplete="name"
                        autoFocus
                        onChange={(event)=>{setPseudo(event.target.value)}}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        onChange={(event)=>{setEmail(event.target.value)}}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        onChange={(event)=>{setPassword(event.target.value)}}
                        autoComplete="current-password"
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="repassword"
                        label="Re-Password"
                        type="password"
                        id="repassword"
                        onChange={(event)=>{setRepassword(event.target.value)}}
                        autoComplete="current-password"
                    />
                    <input
                        accept="image/*"
                        id="contained-button-file"
                        multiple
                        type="file"
                        onChange={(event)=>{ setFile(event.target.files[0])}}
                        style={{display: "none"}}
                    />
                    <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span" style={{marginTop: "1rem"}}>
                            {file.name === undefined ? "Upload" : file.name}
                        </Button>
                    </label>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={!isFormValid}
                    >
                        S'inscrire
                    </Button>
                    {error && (<p style={{color: "red"}}>{error.message}</p>)}
                    <Grid container>
                        <Grid item>
                            <Link to="/login" variant="body2">
                                {"Have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}