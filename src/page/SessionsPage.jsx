import React, { useEffect, useState } from 'react';
import { Button, Card, CardContent, Container, Grid, TextField, Modal } from '@material-ui/core';

import { Link, useHistory} from 'react-router-dom';
import AddSessionForm from '../component/AddSessionForm';
import SessionCard from '../component/SessionCard';

import '../assets/css/SessionsPage.css'
import logo from '../assets/img/eco-surf-logo.svg'
import firebaseConnector from '../api/firebase-connector';
import SideDashboard from '../component/SideDashboard';
import AddSessionBouton from '../component/AddSessionBouton';
import {Add} from "@material-ui/icons";

let monthTrashes = 0
let monthSessions = 0
let sumCleanlinessSessions = 0
let nbSessions = 0

export default function SessionsPage(){
    const [open, setOpen] = React.useState(false);
    const rootRef = React.useRef(null);

    const [listSessionsCard, setListSessionsCard] = useState([]);



    useEffect(()=>{
        firebaseConnector.firestore().collection('sessions').where('user_id', '==', firebaseConnector.auth().currentUser.uid).orderBy('date', 'desc').onSnapshot(
            (snapshot)=>{
                setListSessionsCard(snapshot.docs)
            }
        );
    },[])


    return (
        <Container maxWidth="lg" style={{textAlign: "center"}} className="mobile-container"  ref={rootRef}>
            <Modal
                disablePortal
                disableEnforceFocus
                disableAutoFocus
                aria-labelledby="server-modal-title"
                aria-describedby="server-modal-description"
                container={() => rootRef.current}
                onClose={event => setOpen(false)}
                open={open}
                id="addSectionModal"
            >
                <AddSessionForm afterSubmitCallback={() => setOpen(false)}/>
            </Modal>
            <Grid container spacing={3}>
                <Grid item md={8} spacing={3}>
                    <div style={{paddingTop : '51px'}}>
                        {listSessionsCard.map((feedDoc, index) =>
                            (<FeedCard feedDoc={feedDoc} key={index}/>)
                        )}
                    </div>
                </Grid>

                <Grid item md={4} spacing={3} style={{
                    position: 'sticky'
                }}>
                    <Grid item>
                        <AddSessionBouton openFunction={() => setOpen(true)}/>
                    </Grid>
                    <Grid item>
                        <SideDashboard listSessionsCard={listSessionsCard} nbTrashes={66} nbSessions={66} cleanlinessSessions={66}/>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}


function FeedCard({feedDoc}) {
    const feedData = feedDoc.data();
    var date = new Date(feedData.date.seconds * 1000);
    var now = new Date()
    if(date.getMonth() == now.getMonth() && date.getYear() == now.getYear()){
        monthTrashes +=1
    }
    const options = { year: 'numeric', month: 'long', day: 'numeric', hour : '2-digit', minute : '2-digit' };
    date = date.toLocaleDateString(undefined, options);
    monthSessions+=1;
    sumCleanlinessSessions += feedData.note_proprete;
    nbSessions +=1;
    return (
            <Grid item>
                <SessionCard 
                        pseudo={feedData.user_pseudo}
                        user_id={feedData.user_id}
                        date={date}
                        cleanliness={feedData.note_proprete}
                        numberTrashes={feedData.nb_dechet}
                        hours={Math.floor(feedData.time/60)}
                        minutes={feedData.time%60}
                        description={feedData.description}
                        image1Path={feedData.picture_link}
                        lat={feedData.lat}
                        lng={feedData.lng}
                 />
            </Grid>
    )
}